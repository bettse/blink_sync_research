const fs = require('fs');
const DEC = 10;
const HEX = 16;

function main(filename) {
  const commands = fs.readFileSync(filename, {encoding: 'utf8'})
  let maxSize = 0;
  const addr2data = {};

  commands.split('\n').forEach(line => {
    if (line.length ===0) {
      return;
    }
    //  14206231-14211341 SPI flash/EEPROM: Commands: Fast read data (addr 0x000000, 32 bytes): 10 00 00 ff 00 00 00 00 10 00 00 fd 00 00 00 00 10 00 01 73 00 00 00 00 10 00 01 71 00 00 00 00
    if (line.includes('SPI flash/EEPROM: Commands: Fast read data')) {
      const [junk, useful] = line.split('(');
      const [_, addrStr, lenStr, bytes, ...hex] = useful.split(' ');

      const addr = parseInt(addrStr, HEX);
      const len = parseInt(lenStr, DEC);
      const data = Buffer.from(hex.join(''), 'hex')
      addr2data[addr] = data;
      if (addr > maxSize) {
        maxSize = addr + len;
      }
    } else {
      console.log('non read line:', line);
    }
  })
  const bin = Buffer.alloc(maxSize);

  Object.keys(addr2data).forEach(addr => {
    const data = addr2data[addr];
    const addrVal = parseInt(addr, DEC);
    // Copy `buf1` bytes 16 through 19 into `buf2` starting at byte 8 of `buf2`.
    // buf1.copy(buf2, 8, 16, 20);
    data.copy(bin, addrVal)
  })

  return bin;
}

if (process.argv.length > 2) {
  const filename = process.argv[2]
  const bin = main(filename);
  fs.writeFileSync(filename.replace('.txt', '.bin'), bin);
  console.log(bin.length);
} else {
  console.log("node cmd2bin.js <file>")
}
