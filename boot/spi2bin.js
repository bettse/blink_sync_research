const fs = require('fs');

// In fairness, I wrote this before realizing the SPI EEPROM decoder worked with winbond

const raw_miso = fs.readFileSync('./spi-miso.txt', {encoding: 'utf8'})
const raw_mosi = fs.readFileSync('./spi-mosi.txt', {encoding: 'utf8'})

function main() {
  const paired_commands = {}

  raw_miso.split('\n').forEach(line => {
    const [ts_range, ...rest] = line.split(' ');
    paired_commands[ts_range] = {miso: rest.join(' ')}
  })

  raw_mosi.split('\n').forEach(line => {
    const [ts_range, ...rest] = line.split(' ');
    paired_commands[ts_range] = {...paired_commands[ts_range], mosi: rest.join(' ')}
  })


  const addr2data = {};
  let maxSize = 0;

  Object.keys(paired_commands).forEach(ts => {
    const { miso, mosi } = paired_commands[ts];
    if (miso.length === 0 && mosi.length === 0) {
      return;
    }

    const fullAddr = parseMosiLine(mosi);
    const data = parseMisoLine(miso);

    const addrVal = Buffer.from(fullAddr.padStart(8, '0'), 'hex').readUInt32BE();
    if (addrVal > maxSize) {
      maxSize = addrVal + Buffer.from(data, 'hex').length
    }

    addr2data[fullAddr] = data;
  })

  const bin = Buffer.alloc(maxSize);

  Object.keys(addr2data).forEach(addr => {
    const dataString = addr2data[addr];

    const addrVal = Buffer.from(addr.padStart(8, '0'), 'hex').readUInt32BE();
    const data = Buffer.from(dataString, 'hex')
    // Copy `buf1` bytes 16 through 19 into `buf2` starting at byte 8 of `buf2`.
    // buf1.copy(buf2, 8, 16, 20);
    data.copy(bin, addrVal)
  })

  return bin;
}

function parseMosiLine(mosi) {
  //SPI: MOSI transfers: 0B 01 7C E0 00 00 ...
  const [_spi, _mosi, _transfers, cmd, addr1, addr2, addr3, ...zeros] = mosi.split(' ');
  if (cmd.toLowerCase() !== '0b') {
    console.log('Unexpected cmd', cmd);
  }
  const fullAddr = [addr1, addr2, addr3].join('');
  return fullAddr;
}

function parseMisoLine(miso) {
  // SPI: MISO transfers: 00 00 00 00 00 37 ...
  const [_spi, _miso, _transfers, zero0, zero1, zero2, zero3, zero4, ...data] = miso.split(' ');
  return data.join('')
}

const bin = main();
console.log(bin.toString('hex'))
