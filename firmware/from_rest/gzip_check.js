const fs = require('fs');

const gzipHeader = Buffer.from('1f8b0800', 'hex')
const dest = 'gzips'
const names = fs.readdirSync('.');

names.forEach(name => {
  if (name.endsWith('.bin')) {
    const contents = fs.readFileSync(name);
    if (Buffer.compare(gzipHeader, contents.slice(0, gzipHeader.length)) === 0) {
      fs.renameSync(name, name.replace('bin', 'tar.gz'));
    }
  }
})
