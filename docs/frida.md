# Frida


```javascript

Java.perform(() => {    
  const SMEncryptionData = Java.use("com.immediasemi.blink.utils.SMEncryptionData");
  const aesKey = SMEncryptionData.getInstance().aesKey.value
  const session_key = SMEncryptionData.getInstance().session_key.value
  console.log(JSON.stringify({session_key, aesKey}));
})

Java.perform(() => {
  const AES_INFO_STRING = "aesblinkob";
  const SMEncryptionData = Java.use("com.immediasemi.blink.utils.SMEncryptionData");
  const input = "nOWXBssl2MT3fn-kKV98yQ";
  const aesKey = SMEncryptionData.getInstance().hkdf(input, 16, AES_INFO_STRING);
  //const positiveAesKey = aesKey.map((x) => x > 0 ? x : 256 + x)
  console.log(JSON.stringify({input, aesKey}));
})

//{"input":"","aesKey":[72,-10,96,70,64,101,27,-68,21,124,-81,-24,-11,29,-99,-33]}
//{"input":"nOWXBssl2MT3fn-kKV98yQ","aesKey":[67,-76,-69,94,9,110,84,-26,11,-84,52,28,-77,-43,-9,-7]}

Java.perform(() => {
  const Mac = Java.use("javax.crypto.Mac");
  const SecretKeySpec = Java.use("javax.crypto.spec.SecretKeySpec");
  const a = Java.array('byte', new Array(32).fill(0));
  const secret = Java.array('byte', new Array(0));
  const mac = Mac.getInstance("HmacSHA256");
  mac.init(SecretKeySpec.$new(a, "HmacSHA256"));
  const result = mac.doFinal(secret)
  console.log(JSON.stringify(result))
})


```
