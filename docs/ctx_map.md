# CTX


```c

// ctx is 0x4C4C70 
//ctx length = 184
ctx[0] = "/dev/mtd6" // null terminated?

ctx[30] = 0 // write.  0: disable, 1: enable

ctx[64] = mac
ctx[65] = mac
ctx[66] = mac
ctx[67] = mac
ctx[68] = mac
ctx[69] = mac

ctx[72] = serial
ctx[73] = serial
ctx[74] = serial
ctx[75] = serial

ctx[80] = password_length
ctx[81] = password_length
ctx[82] = password_length
ctx[83] = password_length

ctx[84...] = wifi passwd

ctx[136] = '0' or '1'
ctx[137] = new_key
ctx[141] = new_key
ctx[145] = new_key
ctx[149] = new_key
ctx[153] = dsn serial valid
ctx[154] = dsn serial
ctx[158] = dsn serial
ctx[162] = dsn serial
ctx[166] = dsn serial
ctx[170] = region
ctx[171] = region
ctx[172] = region
ctx[173] = region

```
