These were downloaded from the open source attribution site:
https://apphelp.immedia-semi.com/en/license/Attribution.html


https://apphelp.immedia-semi.com/en/license/oss/build_instructions.txt

```
openwrt
========
tar xzf openwrt-mod.tar.gz
cd openwrt
make


u-boot
======
tar xzf uboot-mod.tar.gz
cd u-boot
modify Makefile to have MAKECMD and PATH point correctly to your crosscompiler
make BlinkSyncModule
```


