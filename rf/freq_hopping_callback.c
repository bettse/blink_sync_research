
/* WARNING (jumptable): Heritage AFTER dead removal. Example location: r0x000185a4 : 0x00013c7c */
/* WARNING: Globals starting with '_' overlap smaller symbols at the same address */
/* WARNING: Restarted to delay deadcode elimination for space: ram */

hrtimer_restart freq_hopping_callback(hrtimer *timer)

{
  undefined4 *puVar1;
  undefined4 *puVar2;
  uchar uVar3;
  uchar uVar4;
  uchar uVar5;
  char cVar6;
  char cVar7;
  uchar uVar8;
  int iVar9;
  hrtimer_restart hVar10;
  uint *puVar11;
  byte bVar16;
  uchar uVar17;
  uint channel;
  int iVar12;
  uchar *puVar13;
  code *pcVar14;
  uint uVar15;
  int iVar18;
  char *pcVar19;
  char cVar20;
  uchar *bytes;
  bool bVar22;
  si4455_arg_t *psVar21;
  undefined4 uVar23;
  uint uVar24;
  uint uVar25;
  uint uVar26;
  int iVar27;
  int iVar28;
  ktime_t kVar29;
  uint local_30;
  uint local_2c;
  
  channel = FATAL_ERROR_STOP_CODE;
  if (FREQ_HOP_ON == 0) {
    iVar9 = FUN_00011438();
    if (iVar9 < 0) {
      printk("change state timeout\n");
    }
    pcVar19 = "timer hit with freq_hop off\n";
LAB_00013574:
    last_state = 0xff;
    printk(pcVar19);
    hVar10 = HRTIMER_NORESTART;
    goto LAB_00015550;
  }
  if (FATAL_ERROR_STOP_CODE != 0) {
    iVar9 = FUN_00011438();
    if (iVar9 < 0) {
      printk("change state timeout\n");
    }
    pcVar19 = "timer hit with fatal error\n";
    goto LAB_00013574;
  }
  now = (**(code **)(*(int *)(timer + 0x24) + 0x14))();
  ns_to_timeval(&local_30);
  new_state = (int)local_2c / 100000;
  _tnow = local_30;
  DAT_000185a4 = local_2c;
  if (err_chip_reset == 0) {
    if (last_state == 0xff) {
      iVar9 = last_state;
      if (new_state == 0) {
LAB_000136ac:
        state_change = 1;
        bVar22 = true;
        if (CMD_ON != 1) {
          listen_mode = 1;
          cmd_mode = 0;
          iVar9 = last_state;
          goto LAB_00015530;
        }
        cmd_mode = CMD_ON;
        listen_mode = 0;
LAB_00014ad4:
        DAT_00019550 = DAT_00019550 + 1;
        last_unit_ID = 0;
        iVar9 = Command_channel_to_use(local_30);
        cur_xmit_chan = iVar9;
        if (DAT_00019300 == 6) {
          md5_buffer[0] = 0;
          md5_buffer[1] = (uchar)(DAT_000191f8 >> 8);
          md5_buffer[2] = (uchar)DAT_000191f8;
          md5_buffer[3] = DAT_000191fc;
          md5_buffer[4] = DAT_000191fd;
          md5_buffer[40] = (uchar)local_30;
          md5_buffer[41] = (uchar)(local_30 >> 8);
          md5_buffer[42] = (uchar)(local_30 >> 0x10);
          md5_buffer[43] = md5_session_key[0];
          md5_buffer[44] = md5_session_key[1];
          md5_buffer[45] = md5_session_key[2];
          md5_buffer[46] = md5_session_key[3];
          md5_buffer[47] = 0x12;
          md5_buffer[48] = '4';
          md5_buffer[49] = 'V';
          md5_buffer[54] = (uchar)DAT_000191f0;
          md5_buffer[53] = (uchar)(DAT_000191f0 >> 8);
          md5_buffer[50] = 'x';
          md5_buffer[51] = (uchar)(DAT_000191f0 >> 0x18);
          md5_buffer[52] = (uchar)(DAT_000191f0 >> 0x10);
          bytes = md5_buffer + 5;
          cur_xmit_chan = iVar9;
          do {
            *bytes = -0x56;
            bytes = bytes + 1;
          } while (bytes != md5_buffer + 0x28);
          compute_md5(md5_out,md5_buffer,0);
          iVar27 = DAT_00019300;
          uVar4 = md5_out[2];
          uVar3 = md5_out[1];
          uVar17 = md5_out[0];
          cVar7 = (char)DAT_000191f0;
          channel = DAT_000191f0 >> 0x18;
          iVar12 = DAT_000191f0 << 8;
          iVar18 = DAT_000191f0 << 0x10;
          cVar6 = md5_out[0] + md5_out[1] + md5_out[2];
          cVar20 = -1;
          if (DAT_00019300 != 0x106) {
            cVar20 = (char)DAT_00019300;
          }
          bytes = cmd_packet_data + 0x274;
          puVar13 = cmd_packet_data + 0x4e4;
          do {
            *bytes = uVar17;
            bytes[1] = uVar3;
            bytes[2] = uVar4;
            bytes[8] = ~(cVar6 + (char)channel + cVar7 + 1 + (char)((uint)iVar12 >> 0x18) + (char)((uint)iVar18 >> 0x18) + cVar20);
            uVar5 = md5_out[3];
            bytes = bytes + 0x10;
          } while (bytes != cmd_packet_data + 0x4e4);
          channel = (DAT_000191f8 & 0xff) + (uint)md5_out[3] + 1 + (uint)DAT_000191fc + ((DAT_000191f8 << 0x10) >> 0x18);
          bytes = &DAT_000191fd;
          bVar22 = false;
          uVar24 = channel;
          while (bytes != (byte *)((int)&DAT_000191f8 + iVar27)) {
            bVar22 = true;
            uVar24 = uVar24 + (uint)*bytes;
            bytes = bytes + 1;
          }
          if (bVar22) {
            channel = uVar24;
          }
          chksum = ~channel & 0xff;
          do {
            *puVar13 = uVar5;
            puVar13[5] = (uchar)~channel;
            puVar13 = puVar13 + 0xd;
          } while (puVar13 != sr_packet_data + 6);
        }
        if (eu_mode == 0) {
          iVar18 = (uint)DAT_000191e5 * 0x100 + (uint)DAT_000191e4;
LAB_00014d70:
          iVar12 = cmd_bytes_to_send;
          iVar9 = FUN_00011830(iVar18,cmd_packet_data,cmd_bytes_to_send,iVar9);
          if (-1 < iVar9) {
            if (iVar12 != 0) {
              printk("cmd start OK - command: %d  chan %d cmd id %d\n",(uint)DAT_000191fc,cur_xmit_chan,DAT_000191f8);
            }
LAB_00014dcc:
            kVar29 = (**(code **)(*(int *)(timer + 0x24) + 0x14))();
            last_state = new_state;
            now = kVar29;
            hrtimer_forward(timer,new_state,(int)((ulonglong)kVar29 >> 0x20),(int)kVar29,ktime._0_4_,ktime._4_4_);
            hVar10 = HRTIMER_RESTART;
            goto LAB_00015550;
          }
          pcVar19 = "send cmd failed\n";
        }
        else {
          iVar12 = FUN_000116b4(iVar9);
          if (-1 < iVar12) {
            iVar18 = (uint)DAT_000191e5 * 0x100 + (uint)DAT_000191e4;
            iVar9 = cur_xmit_chan;
            if (iVar12 != 0) goto LAB_00014d70;
            iVar9 = FUN_00011830(iVar18,cmd_packet_data,0,cur_xmit_chan);
            if (iVar9 < 0) {
              pcVar19 = "send cmd failed\n";
              goto LAB_00014d8c;
            }
            goto LAB_00014dcc;
          }
          pcVar19 = "clear channel assesment failed\n";
        }
LAB_00014d8c:
        printk(pcVar19);
        _err_cnts = _err_cnts + 1;
        err_restart(timer);
        hVar10 = HRTIMER_RESTART;
        goto LAB_00015550;
      }
      goto LAB_000153f8;
    }
    if (new_state == last_state) {
LAB_0001369c:
      bVar22 = false;
      state_change = 0;
      iVar9 = new_state;
    }
    else {
      state_change = 1;
      if (new_state == 0) goto LAB_000136ac;
      bVar22 = true;
      iVar9 = last_state;
    }
LAB_000136e8:
    if (listen_mode != 1) {
      if (new_state == 9) {
LAB_00015530:
        if (BEACON_REQUEST != 0) goto LAB_0001376c;
LAB_000154b0:
        iVar12 = err_chip_reset;
        switch(new_state) {
        case 0:
          goto switchD_0001378c_caseD_0;
        case 1:
        case 2:
        case 3:
          goto switchD_0001378c_caseD_1;
        case 4:
          uVar24 = 0;
          goto switchD_0001378c_caseD_4;
        case 5:
        case 6:
        case 7:
          break;
        case 8:
          goto switchD_0001378c_caseD_8;
        case 9:
          goto switchD_0001378c_caseD_9;
        default:
          goto switchD_00014ac4_caseD_9;
        case 0xe:
          goto switchD_0001378c_caseD_e;
        case 0xf:
        case 0x10:
        case 0x11:
          goto switchD_0001378c_caseD_f;
        }
LAB_0001542c:
        if (*(uint *)(&srt + (DAT_00018b9c + 0x42) * 4) < DAT_000185a4) {
          DAT_00018b9c = DAT_00018b9c + 1;
          if (DAT_00018b9c < _srt) {
            sync_reply_trigger = 1;
          }
          goto LAB_00014a28;
        }
        iVar9 = *(int *)(timer + 0x24);
        goto LAB_00014a2c;
      }
      if (cmd_mode != 1) {
        printk("illegal state 2\n");
        hVar10 = HRTIMER_NORESTART;
        goto LAB_00015550;
      }
      switch(new_state) {
      case 0:
        if (bVar22) goto LAB_00014ad4;
        break;
      case 1:
      case 2:
      case 3:
        break;
      case 4:
        if (!bVar22) goto switchD_00014ac4_caseD_5;
        ACK_RECEIVED = 0;
        iVar9 = FUN_00011678();
        if (-1 < iVar9) goto LAB_0001518c;
        pcVar19 = "reset_fifo timeout\n";
        goto LAB_000151c4;
      case 5:
      case 6:
      case 7:
switchD_00014ac4_caseD_5:
        iVar9 = FUN_000114b0();
        if (-1 < iVar9) {
          iVar9 = new_state;
          if (fifo_info[0] < 8) goto LAB_000153f8;
          SI4455_read_rx_fifo(8);
          uVar8 = rx_fifo_data[6];
          uVar5 = rx_fifo_data[5];
          uVar4 = rx_fifo_data[4];
          uVar3 = rx_fifo_data[3];
          uVar17 = rx_fifo_data[2];
          unit_ID = (uint)rx_fifo_data[0];
          uVar24 = (uint)rx_fifo_data[3];
          uVar25 = (uint)rx_fifo_data[6];
          if ((~(uVar25 + (uint)rx_fifo_data[5] + (uint)rx_fifo_data[4] + uVar24 + (uint)rx_fifo_data[2] + unit_ID + (uint)rx_fifo_data[1] + 1) & 0xff) == (uint)rx_fifo_data[7]) {
            uVar15 = unit_ID - 1;
            if (uVar15 < 0x20) {
              uVar26 = 1 << (uVar15 & 0x1f);
              ACK_RECEIVED = 1;
              if ((DAT_00019008 & uVar26) == 0) {
                iVar9 = uVar15 * 6;
                DAT_00019008 = DAT_00019008 | uVar26;
                DAT_00019004 = DAT_00019004 & ~uVar26;
                (&DAT_00019010)[iVar9] = rx_fifo_data[1];
                (&DAT_00019011)[iVar9] = uVar17;
                (&DAT_00019012)[iVar9] = uVar3;
                (&DAT_00019013)[iVar9] = uVar4;
                (&DAT_00019014)[iVar9] = uVar5;
                (&DAT_00019015)[iVar9] = uVar8;
                channel = uVar25;
LAB_000150c0:
                iVar9 = uVar15 + 0x36;
              }
              else {
                iVar9 = uVar15 * 6;
                if (((((uint)(byte)(&DAT_00019010)[iVar9] != (uint)rx_fifo_data[1]) || ((uint)(byte)(&DAT_00019011)[iVar9] != (uint)rx_fifo_data[2])) || ((uint)(byte)(&DAT_00019012)[iVar9] != uVar24))
                   || (((uint)(byte)(&DAT_00019013)[iVar9] != (uint)rx_fifo_data[4] || ((uint)(byte)(&DAT_00019014)[iVar9] != (uint)rx_fifo_data[5])))) {
LAB_00015044:
                  DAT_00019004 = DAT_00019004 | uVar26;
                  DAT_00019008 = ~uVar26 & DAT_00019008;
                  printk("second ack doesn\'t agree\n");
                  uVar24 = (uint)rx_fifo_data[3];
                  uVar15 = unit_ID - 1;
                  channel = (uint)rx_fifo_data[6];
                  goto LAB_000150c0;
                }
                channel = (uint)(byte)(&DAT_00019015)[uVar15 * 6];
                iVar9 = unit_ID + 0x35;
                if (uVar25 != channel) goto LAB_00015044;
              }
              repeat_sequence_count = channel;
              *(uint *)(&cmd_status + iVar9 * 4) = uVar24;
              iVar9 = FUN_00011474();
              channel = unit_ID;
              if (iVar9 < 0) goto LAB_0001519c;
              uVar24 = (uint)fifo_info[3];
              bVar22 = unit_ID != last_unit_ID;
              *(uint *)(&cmd_status + (unit_ID + 0x55) * 4) = uVar24;
              if (bVar22) {
                printk("ack cmd unit %d l %d in %d, out %d, ch %d, afc %d, stat %d\n",channel,lost_ack,uVar24,*(undefined4 *)(&cmd_status + (channel + 0x35) * 4),cur_xmit_chan,repeat_sequence_count,
                       (uint)rx_fifo_data[1]);
                last_unit_ID = unit_ID;
              }
            }
            else {
              printk("bad unit ID #\n");
            }
          }
          else {
            printk("bad checksum: %x, %x, %x , %x, %x, %x, %x, %x\n");
          }
          iVar9 = new_state;
          if (_DAT_0000842c != 0) goto LAB_000153f8;
          iVar9 = FUN_00011678();
          if (iVar9 < 0) goto LAB_00015184;
LAB_0001518c:
          iVar9 = FUN_00011474(0x10000);
          if (iVar9 < 0) goto LAB_0001519c;
          channel = FUN_0001053c(_tnow);
          iVar12 = SI4455_start_rx_size(channel,8);
          iVar9 = new_state;
          if (-1 < iVar12) goto LAB_000153f8;
          pcVar19 = "start rx error:\n";
          goto LAB_000151c4;
        }
        goto LAB_00014ef8;
      case 8:
        iVar9 = new_state;
        if (bVar22) {
          iVar9 = FUN_00011438();
          if (iVar9 < 0) {
            printk("change state timeout\n");
          }
          if (ACK_RECEIVED == 0) {
            printk("no cmd ack\n");
            lost_ack = lost_ack + 1;
            DAT_0001955c = DAT_0001955c + 1;
          }
          cmd_passes = cmd_passes + 1;
          if ((DAT_00019004 & 0xffff) == 0) {
            if ((DAT_00019004 & 0xffff0000) != 0) {
              bVar22 = false;
              channel = 0x10;
              uVar24 = 0x10000;
              iVar9 = DAT_000191d0;
              do {
                if ((uVar24 & DAT_00019004) != 0) {
                  iVar9 = iVar9 + (1 << ((channel & 0xf) << 1));
                  bVar22 = true;
                }
                channel = channel + 1;
                uVar24 = 1 << (channel & 0x1f);
              } while (channel != 0x20);
              if (bVar22) {
                DAT_000191d0 = iVar9;
              }
            }
          }
          else {
            bVar22 = false;
            iVar9 = DAT_0001900c;
            do {
              if ((1 << (channel & 0x1f) & DAT_00019004) != 0) {
                iVar9 = iVar9 + (1 << ((channel & 0xf) << 1));
                bVar22 = true;
              }
              channel = channel + 1;
            } while (channel != 0x10);
            if (bVar22) {
              DAT_0001900c = iVar9;
            }
          }
          printk("pending %d passes %d\n");
          if ((DAT_00019004 == 0) || (2 < cmd_passes)) {
            _cmd_status = 0;
            CMD_ON = 0;
          }
          else {
            DAT_000191f0 = DAT_00019004;
          }
          iVar9 = new_state;
          if (err_chip_reset != 0) {
            iVar9 = FUN_00010a38();
            if (iVar9 < 0) {
              DAT_00019544 = DAT_00019544 + 1;
              iVar9 = FUN_00010a38();
              if (iVar9 < 0) {
                DAT_00019544 = DAT_00019544 + 1;
                FATAL_ERROR_STOP_CODE = 0xfe;
              }
            }
            if (err_chip_reset != 0) {
              DAT_00019548 = DAT_00019548 + 1;
            }
            err_chip_reset = 0;
            iVar9 = new_state;
          }
        }
        goto LAB_000153f8;
      default:
        goto switchD_00014ac4_caseD_9;
      }
      if ((bytes_sent < 1) || (iVar9 = cmd_bytes_to_send, cmd_bytes_to_send <= bytes_sent)) {
        pcVar14 = *(code **)(*(int *)(timer + 0x24) + 0x14);
LAB_00014e88:
        last_state = new_state;
        kVar29 = (*pcVar14)(0x20000,iVar9);
        now = kVar29;
        hrtimer_forward(timer);
        hVar10 = HRTIMER_RESTART;
        goto LAB_00015550;
      }
      iVar9 = FUN_000114fc(cmd_packet_data);
      if (-1 < iVar9) {
        pcVar14 = *(code **)(*(int *)(timer + 0x24) + 0x14);
        iVar9 = bytes_sent;
        goto LAB_00014e88;
      }
      printk("cnt tx pckts err3 %d\n");
      goto LAB_000151d0;
    }
    if (BEACON_REQUEST == 0) {
      if (new_state == 4) {
        new_state = 0xe;
        goto switchD_0001378c_caseD_e;
      }
      if (new_state == 5) {
        new_state = 0xf;
      }
      else {
        if (new_state == 6) {
          new_state = 0x10;
        }
        else {
          if (new_state != 7) goto LAB_000154b0;
          new_state = 0x11;
        }
      }
      goto switchD_0001378c_caseD_f;
    }
LAB_0001376c:
    iVar12 = err_chip_reset;
    uVar24 = BEACON_REQUEST;
    switch(new_state) {
    case 0:
switchD_0001378c_caseD_0:
      iVar12 = err_chip_reset;
      if (bVar22) {
        BEACON_REQUEST = 0;
        memset(&srt,0,0x148);
        SI4455_stream_bytes((uchar *)0x83b4,1);
        iVar9 = FUN_000107f4(10000,2,data_in);
        if (iVar9 < 0) {
          pcVar19 = "get device state timeout:\n";
        }
        else {
          iVar9 = FUN_00011678(0x10000);
          if (iVar9 < 0) {
LAB_000137fc:
            pcVar19 = "reset_fifo timeout\n";
          }
          else {
            _DAT_000083d8 = 0x8eef;
            SI4455_stream_bytes((uchar *)0x83d4,6);
            iVar9 = FUN_000107f4(25000,0,data_in);
            if (iVar9 < 0) goto LAB_0001415c;
            iVar9 = FUN_00011474(0x10000);
            if (iVar9 < 0) {
              pcVar19 = "modem status timeout\n";
            }
            else {
              channel = FUN_000105b4(_tnow);
              iVar9 = SI4455_start_rx_size(channel,4);
              iVar12 = 0x10000;
              if (-1 < iVar9) goto switchD_0001378c_caseD_1;
LAB_00013aec:
              pcVar19 = "start rx error:\n";
            }
          }
        }
        goto LAB_00013800;
      }
    case 1:
    case 2:
    case 3:
switchD_0001378c_caseD_1:
      iVar9 = FUN_000114b0(iVar12);
      if (-1 < iVar9) {
        do {
          iVar9 = new_state;
          if (fifo_info[0] < 4) goto LAB_000153f8;
          SI4455_read_rx_fifo(4);
          device_serial0 = (uint)rx_fifo_data[0];
          device_serial1 = (uint)rx_fifo_data[1];
          device_serial = device_serial0 * 0x100 + device_serial1;
          iVar9 = BEACON_REQUEST;
          if ((uint)rx_fifo_data[3] == (~(device_serial0 + device_serial1 + (uint)rx_fifo_data[2] + 1) & 0xff)) {
            if (verbose_mode != 0) {
              printk(&LC42);
            }
            channel = device_serial;
            iVar12 = 0;
            bVar22 = false;
            puVar11 = (uint *)&onboard_status;
            iVar9 = BEACON_REQUEST;
            do {
              uVar24 = *puVar11;
              if (uVar24 == channel) {
                uVar15 = puVar11[2];
                puVar11[1] = puVar11[1] + 1;
                uVar25 = DAT_000185a4;
                if (uVar15 != 0) {
                  if ((_srt == 0) || (*(uint *)(&srt + (_srt + 0x41) * 4) < DAT_000185a4 + 0x591c8)) {
                    iVar28 = _srt + 2;
                    iVar27 = _srt + 0x22;
                    iVar18 = _srt + 0x12;
                    iVar9 = _srt + 0x42;
                    *(uint *)(&srt + (_srt + 0x32) * 4) = DAT_000185a4 + 0x591c8;
                    _srt = _srt + 1;
                    *(uint *)(&srt + iVar28 * 4) = uVar24;
                    *(uint *)(&srt + iVar27 * 4) = uVar15;
                    *(uint *)(&srt + iVar18 * 4) = uVar25;
                    *(uint *)(&srt + iVar9 * 4) = uVar25 + 0x6a338;
                  }
                  bVar16 = rx_fifo_data[2] >> 7;
                  (&DAT_00018d04)[iVar12 * 6] = (&DAT_00018d04)[iVar12 * 6] + (uint)bVar16;
                  unit_ID = uVar15;
                  (&DAT_00018d08)[iVar12 * 6] = (uint)bVar16;
                  BEACON_REQUEST = 1;
                  printk("serial %x, sequence: %x\n",uVar24);
                  goto LAB_00013ab4;
                }
                iVar9 = 0;
                bVar22 = true;
              }
              iVar12 = iVar12 + 1;
              puVar11 = puVar11 + 6;
            } while (iVar12 != 0x20);
            if (bVar22) goto LAB_00013ab0;
          }
          else {
LAB_00013ab0:
            BEACON_REQUEST = iVar9;
          }
LAB_00013ab4:
          if (_DAT_0000842c == 0) {
            iVar9 = FUN_00011678();
            if (iVar9 < 0) goto LAB_000137fc;
            channel = FUN_000105b4(_tnow);
            iVar9 = SI4455_start_rx_size(channel,4);
            if (iVar9 < 0) goto LAB_00013aec;
          }
          iVar9 = FUN_000114b0();
        } while (-1 < iVar9);
      }
LAB_00013fa4:
      pcVar19 = "fifo info timeout\n";
LAB_00013800:
      printk(pcVar19);
      DAT_00019534 = DAT_00019534 + 1;
      err_restart(timer);
      hVar10 = HRTIMER_RESTART;
      break;
    case 4:
switchD_0001378c_caseD_4:
      if (!bVar22) goto switchD_0001378c_caseD_5;
      bytes_sent = 0;
      special_beacon = 0;
      while (iVar9 = channel * 4, channel < _srt) {
        puVar1 = &DAT_00018ba0 + channel;
        puVar2 = &DAT_00018c20 + channel;
        channel = channel + 1;
        printk("sync rqst - unitID: %d, serial:%x, time:%d\n",*puVar2,*puVar1,*(undefined4 *)(&DAT_00018be0 + iVar9));
      }
      iVar9 = FUN_00011438();
      if (iVar9 < 0) {
LAB_00014108:
        pcVar19 = "change state timeout\n";
        goto LAB_00013800;
      }
      iVar9 = new_state;
      if (BEACON_REQUEST != 1) goto LAB_000153f8;
      time_to_next_beacon = ((((int)_tnow / 5) * 5 - _tnow) + 5) * 0x32 + 0x19;
      DAT_00019554 = DAT_00019554 + 1;
      if (0xfa < time_to_next_beacon) {
        time_to_next_beacon = 0x19;
      }
      beacon_freq = FUN_0001063c(_tnow,time_to_next_beacon);
      iVar9 = 0x20000;
      if (FIXED_CHANNEL != 0) {
        channel = Command_channel_to_use(_tnow);
        *(uint *)(iVar9 + -0x7478) = channel | 0x80;
      }
      sync_reply_trigger = 1;
      DAT_00018b9c = 0;
      if (BEACON_REQUEST != 1) goto LAB_0001542c;
      iVar9 = 0x32;
LAB_00013ca4:
      uVar24 = sync_reply_trigger;
      if (DAT_000185a4 <= *(uint *)(&srt + iVar9 * 4)) {
LAB_00013cbc:
        sync_reply_trigger = uVar24;
        goto LAB_00013cc0;
      }
      sync_reply_trigger = 0;
      iVar12 = FUN_00011678();
      iVar9 = time_to_next_beacon;
      if (-1 < iVar12) {
        sr_packet_offset = (int)(DAT_000185a4 - 400000) / 0xaa0;
        if (sr_packet_offset < 0x87) {
          channel = sr_packet_offset + 0x14;
        }
        else {
          sr_packet_offset = 0x86;
          channel = 0x9a;
        }
        uVar24 = sr_packet_offset;
        uVar23 = *(undefined4 *)(&srt + (DAT_00018b9c + 0x22) * 4);
        cVar6 = sr_packet_data[9] + (uchar)uVar23 + 1 + sr_packet_data[10] + sr_packet_data[11];
        uVar5 = (uchar)time_to_next_beacon;
        uVar4 = (uchar)*(undefined4 *)(&srt + (DAT_00018b9c + 2) * 4);
        uVar3 = (uchar)beacon_freq;
        uVar17 = (uchar)((uint)*(undefined4 *)(&srt + (DAT_00018b9c + 2) * 4) >> 8);
        bytes = sr_packet_data + sr_packet_offset * 0x11;
        iVar12 = sr_packet_offset * 0xaa0;
        uVar25 = sr_packet_offset;
        while (uVar25 < channel) {
          bytes[7] = uVar17;
          bytes[8] = uVar4;
          bytes[0xc] = (uchar)uVar23;
          uVar8 = (uchar)(iVar12 / 2000);
          bytes[0xd] = uVar5;
          bytes[0xe] = uVar3;
          bytes[0xf] = uVar8;
          bytes[0x10] = ~(uVar8 + cVar6 + uVar5 + uVar3 + uVar4 + uVar17);
          uVar25 = uVar25 + 1;
          bytes = bytes + 0x11;
          iVar12 = iVar12 + 0xaa0;
        }
        bytes_sent = 0x40;
        if (iVar9 != 0x19) {
          special_beacon = 1;
        }
        channel = ((uint)((ulonglong)_tnow * 0xaaaaaaab >> 0x20) & 0xfffffffe) + _tnow / 3;
        if (sing_mode == 0) {
          if (_tnow == channel) {
            uVar23 = 10;
          }
          else {
            uVar23 = 0x14;
            if (_tnow - channel != 1) {
              uVar23 = 0x1e;
            }
          }
        }
        else {
          if (_tnow == channel) {
            uVar23 = 0x33;
          }
          else {
            uVar23 = 0x36;
            if (_tnow - channel != 1) {
              uVar23 = 0x39;
            }
          }
        }
        if (eu_mode != 0) {
          iVar9 = FUN_000116b4(uVar23);
          if (iVar9 < 0) {
            pcVar19 = "clear channel assesment failed\n";
            goto LAB_00013800;
          }
          if (iVar9 == 0) {
            bytes_sent = 0;
            uVar24 = sr_packet_offset;
          }
          else {
            bytes_sent = 0x40;
            uVar24 = sr_packet_offset;
          }
        }
        SI4455_write_tx_fifo(sr_packet_data + uVar24 * 0x11,0x40);
        DAT_00008413 = (undefined)((uint)sync_reply_bytes_to_send >> 8);
        DAT_00008414 = (undefined)sync_reply_bytes_to_send;
        if (0 < bytes_sent) {
          iVar9 = FUN_00010930(uVar23);
          if (iVar9 < 0) {
            pcVar19 = "start beacon error2:\n";
            goto LAB_00013800;
          }
        }
LAB_0001497c:
        kVar29 = (**(code **)(*(int *)(timer + 0x24) + 0x14))();
        last_state = new_state;
        iVar9 = last_state;
        now = kVar29;
        goto LAB_000153f8;
      }
      pcVar19 = "reset_fifo timeout2\n";
      goto LAB_00013800;
    case 5:
    case 6:
    case 7:
switchD_0001378c_caseD_5:
      if (uVar24 != 1) goto LAB_0001542c;
      if (sync_reply_trigger == 1) {
        iVar9 = DAT_00018b9c + 0x32;
        goto LAB_00013ca4;
      }
      if ((*(uint *)(&srt + (DAT_00018b9c + 0x42) * 4) < local_2c) && (DAT_00018b9c = DAT_00018b9c + 1, DAT_00018b9c < _srt)) goto LAB_00013cbc;
LAB_00013cc0:
      if (bytes_sent < 1) {
        iVar9 = *(int *)(timer + 0x24);
LAB_00014a2c:
        pcVar14 = *(code **)(iVar9 + 0x14);
        last_state = new_state;
        goto LAB_00014a54;
      }
      if (sync_reply_bytes_to_send <= bytes_sent) {
LAB_00014a28:
        iVar9 = *(int *)(timer + 0x24);
        goto LAB_00014a2c;
      }
      iVar9 = FUN_000114b0();
      if (iVar9 < 0) goto LAB_00013fa4;
      if (0x3f < fifo_info[1]) {
        printk("fifo emptied1\n");
        DAT_00019540 = DAT_00019540 + 1;
        err_restart(timer);
        hVar10 = HRTIMER_RESTART;
        break;
      }
      if (1 < fifo_info[1]) {
        channel = (uint)fifo_info[1];
        if (sync_reply_bytes_to_send - bytes_sent <= (int)(uint)fifo_info[1]) {
          channel = sync_reply_bytes_to_send - bytes_sent;
        }
        SI4455_write_tx_fifo(sr_packet_data + sr_packet_offset * 0x11 + bytes_sent,channel);
        bytes_sent = bytes_sent + channel;
        last_state = new_state;
        kVar29 = (**(code **)(*(int *)(timer + 0x24) + 0x14))();
        iVar9 = last_state;
        now = kVar29;
        goto LAB_000153f8;
      }
      SI4455_stream_bytes((uchar *)0x83b4,1);
      iVar9 = FUN_000107f4(10000,2,data_in);
      if (iVar9 < 0) {
        pcVar19 = "get device state timeout3a:\n";
      }
      else {
        DAT_00008413 = (undefined)((uint)(sync_reply_bytes_to_send - bytes_sent) >> 8);
        DAT_00008414 = (undefined)(sync_reply_bytes_to_send - bytes_sent);
        iVar9 = FUN_00010930((uint)data_in[1]);
        if (-1 < iVar9) goto LAB_00014a28;
        pcVar19 = "start tx error2a:\n";
      }
      goto LAB_00013800;
    case 8:
switchD_0001378c_caseD_8:
      if (err_chip_reset != 0) {
        iVar9 = FUN_00010a38();
        if (iVar9 < 0) {
          DAT_00019544 = DAT_00019544 + 1;
          iVar9 = FUN_00010a38();
          if (iVar9 < 0) {
            DAT_00019544 = DAT_00019544 + 1;
            FATAL_ERROR_STOP_CODE = 0xfe;
          }
        }
        if (err_chip_reset != 0) {
          DAT_00019548 = DAT_00019548 + 1;
        }
        err_chip_reset = 0;
      }
      if (last_state != 0x11) {
        iVar9 = new_state;
        if ((last_state != 8) || (iVar9 = new_state, async_mask == 0)) goto LAB_000153f8;
        if (bytes_sent - 1U < 0x22f) {
          iVar9 = FUN_000114fc(async_group_ack_data,0x230);
          if (iVar9 < 0) {
            pcVar19 = "cnt async_ack pckts err1 %d\n";
            iVar9 = bytes_sent;
            goto LAB_000149fc;
          }
          goto LAB_00014a28;
        }
        async_mask = 0;
        iVar9 = FUN_00011438(0x20000);
        if (-1 < iVar9) goto LAB_00014a28;
        printk("change state timeout\n");
        iVar9 = *(int *)(timer + 0x24);
        goto LAB_00014a2c;
      }
      iVar9 = FUN_00011438();
      if (iVar9 < 0) {
        printk("change state timeout\n");
      }
      iVar9 = new_state;
      if (async_mask == 0) goto LAB_000153f8;
      async_group_ack_data[11] = (uchar)(async_mask >> 0x10);
      async_group_ack_data[12] = (uchar)(async_mask >> 8);
      async_group_ack_data[10] = (uchar)(async_mask >> 0x18);
      async_group_ack_data[13] = (uchar)async_mask;
      cVar6 = async_group_ack_data[11] + async_group_ack_data[10] + async_group_ack_data[12] + async_group_ack_data[13];
      async_group_ack_data[7] = 0;
      async_group_ack_data[8] = 0;
      async_group_ack_data[9] = 0;
      async_group_ack_data._14_2_ = 0;
      bytes = async_group_ack_data + 0x17;
      do {
        *bytes = async_group_ack_data[7];
        puVar13 = bytes + 0x10;
        bytes[1] = async_group_ack_data[8];
        bytes[2] = async_group_ack_data[9];
        bytes[3] = async_group_ack_data[10];
        bytes[4] = async_group_ack_data[11];
        bytes[5] = async_group_ack_data[12];
        bytes[6] = async_group_ack_data[13];
        bytes[8] = ~(cVar6 + 1U);
        bytes[7] = async_group_ack_data[14];
        bytes = puVar13;
      } while (puVar13 != cmd_packet_data + 7);
      if ((FIXED_CHANNEL & 0x80) == 0) {
        channel = ((uint)network_id_3 ^ _tnow ^ _tnow >> 6) + 2;
        if (sing_mode == 0) {
          uVar24 = (channel & 0x3f) + 6;
        }
        else {
          channel = channel & 7;
          uVar24 = channel + 0x32;
          if (channel == 0) {
            uVar24 = 0x34;
          }
        }
      }
      else {
        uVar24 = FIXED_CHANNEL + 2 & 0x7f;
      }
      iVar9 = FUN_00011830((uint)DAT_00019375 * 0x100 + (uint)DAT_00019374,async_group_ack_data,0x230,uVar24);
      if (iVar9 < 0) {
        pcVar19 = "start async_ack CTS fail %d\n";
        iVar9 = beacon_freq;
LAB_000149fc:
        printk(pcVar19,iVar9);
        DAT_00019530 = DAT_00019530 + 1;
        err_restart(timer);
        hVar10 = HRTIMER_RESTART;
        break;
      }
      goto LAB_0001497c;
    case 9:
switchD_0001378c_caseD_9:
      if (((local_30 != ((int)local_30 / 5) * 5) && (special_beacon != 1)) || (_DAT_00008424 == 0)) {
        iVar9 = 9;
        goto LAB_000153f8;
      }
      if (1 < iVar9 - 8U) {
        printk("illegal state transition 1: last %d\n");
        DAT_0001953c = DAT_0001953c + 1;
        err_restart(timer);
        hVar10 = HRTIMER_RESTART;
        break;
      }
      if (iVar9 == 8) {
        DAT_0001954c = DAT_0001954c + 1;
        if (b_packet_data[553] != b_packet_data[7]) {
          printk(" beacon packet corrupt: %d \n");
          printk("beacon data bad,%x,%x,%x, %x,%x,%x,%x,%x",(uint)b_packet_data[0],(uint)b_packet_data[1],(uint)b_packet_data[2],(uint)b_packet_data[3],(uint)b_packet_data[4],(uint)b_packet_data[5],
                 (uint)b_packet_data[6],(uint)b_packet_data[7]);
          DAT_00019558 = DAT_00019558 + 1;
        }
        uVar24 = _tnow;
        channel = ((_tnow << 8) >> 0x18) + ((_tnow << 0x10) >> 0x18) + (_tnow & 0xff) + ((uint)(byte)beacon_cycle_code - 0x6f) & 0xff;
        b_packet_data[12] = ~(byte)channel;
        b_packet_data[7] = (byte)beacon_cycle_code + 0x90;
        channel = ~(channel + 1);
        b_packet_data[8] = (uchar)(_tnow & 0xff);
        b_packet_data[10] = (uchar)((_tnow << 8) >> 0x18);
        b_packet_data[0] = DAT_00019368;
        b_packet_data[1] = DAT_00019369;
        b_packet_data[2] = DAT_0001936a;
        b_packet_data[3] = DAT_0001936b;
        b_packet_data[4] = DAT_0001936c;
        b_packet_data[5] = DAT_00019374;
        b_packet_data[6] = DAT_00019375;
        b_packet_data[9] = (uchar)((_tnow << 0x10) >> 0x18);
        b_packet_data[11] = 0;
        bytes = b_packet_data;
        iVar9 = 1;
        do {
          bytes = bytes + 0xd;
          uVar25 = channel & 0xff;
          *bytes = b_packet_data[0];
          bytes[1] = b_packet_data[1];
          channel = uVar25 - 1;
          bytes[2] = b_packet_data[2];
          bytes[3] = b_packet_data[3];
          bytes[4] = b_packet_data[4];
          bytes[5] = b_packet_data[5];
          bytes[6] = b_packet_data[6];
          bytes[7] = b_packet_data[7];
          bytes[8] = b_packet_data[8];
          bytes[9] = b_packet_data[9];
          uVar17 = b_packet_data[10];
          bytes[0xb] = (uchar)iVar9;
          iVar9 = iVar9 + 1;
          bytes[0xc] = (uchar)uVar25;
          bytes[10] = uVar17;
        } while (iVar9 != 0x2b);
        if (special_beacon == 0) {
          beacon_freq = FUN_0001063c(uVar24,0x19);
          printk("b: %d\n",beacon_freq);
        }
        else {
          if (FIXED_CHANNEL != 0) {
            beacon_freq = beacon_freq + 2;
          }
          printk("spec beacon freq: %d\n");
        }
        iVar9 = beacon_bytes_to_send;
        if (eu_mode == 0) {
LAB_00014930:
          iVar9 = FUN_00011830((uint)DAT_00019375 * 0x100 + (uint)DAT_00019374,b_packet_data,iVar9,beacon_freq);
          if (-1 < iVar9) goto LAB_0001497c;
          printk("start beacon CTS fail %d\n",beacon_freq);
        }
        else {
          iVar9 = FUN_000116b4(beacon_freq);
          if (-1 < iVar9) {
            if (iVar9 == 0) {
              beacon_bytes_to_send = 0;
            }
            else {
              beacon_bytes_to_send = 0x22f;
              iVar9 = 0x22f;
            }
            goto LAB_00014930;
          }
          printk("clear channel assesment failed\n");
        }
        DAT_00019530 = DAT_00019530 + 1;
        err_restart(timer);
        hVar10 = HRTIMER_RESTART;
        break;
      }
      if ((0 < bytes_sent) && (bytes_sent < beacon_bytes_to_send)) {
        iVar9 = FUN_000114fc(b_packet_data);
        if (-1 < iVar9) goto LAB_00014a28;
        pcVar19 = "cnt tx pckts err1 %d\n";
        iVar9 = bytes_sent;
        goto LAB_000149fc;
      }
      pcVar14 = *(code **)(*(int *)(timer + 0x24) + 0x14);
      last_state = 9;
LAB_00014a54:
      kVar29 = (*pcVar14)();
      iVar9 = last_state;
      now = kVar29;
      goto LAB_000153f8;
    default:
switchD_00014ac4_caseD_9:
      printk("illegal state \n");
      hVar10 = HRTIMER_NORESTART;
      break;
    case 0xe:
switchD_0001378c_caseD_e:
      if ((bVar22) && (iVar9 != 0xe)) {
        async_mask = 0;
        bytes_sent = 0;
        special_beacon = 0;
        iVar9 = FUN_00011438();
        if (iVar9 < 0) goto LAB_00014108;
        psVar21 = &b;
        bVar16 = swapy(DAT_00019374);
        bytes = (uchar *)0x83d4;
        _DAT_000083d8 = _DAT_000083d8 & 0xff | (ushort)bVar16 << 8;
        uVar17 = swapy((uchar)psVar21[0x11]);
        bytes[5] = uVar17;
        SI4455_stream_bytes(bytes,6);
        iVar9 = FUN_000107f4(25000,0,data_in);
        if (iVar9 < 0) {
LAB_0001415c:
          pcVar19 = "sync bytes timeout:\n";
          goto LAB_00013800;
        }
        iVar9 = FUN_00011678(0x10000);
        if (iVar9 < 0) {
LAB_00015184:
          pcVar19 = "reset_fifo timeout\n";
          goto LAB_000151c4;
        }
        iVar9 = FUN_00011474(0x10000);
        if (-1 < iVar9) {
          channel = FUN_0001053c(_tnow);
          iVar12 = SI4455_start_rx_size(channel,10);
          iVar9 = new_state;
          if (iVar12 < 0) {
            pcVar19 = "start rx error:\n";
            goto LAB_000151c4;
          }
          goto LAB_000153f8;
        }
LAB_0001519c:
        pcVar19 = "modem status timeout\n";
        goto LAB_000151c4;
      }
    case 0xf:
    case 0x10:
    case 0x11:
switchD_0001378c_caseD_f:
      iVar9 = FUN_000114b0();
      if (iVar9 < 0) {
LAB_00014ef8:
        pcVar19 = "fifo info timeout\n";
LAB_000151c4:
        printk(pcVar19);
LAB_000151d0:
        _err_cnts = _err_cnts + 1;
        err_restart(timer);
        hVar10 = HRTIMER_RESTART;
      }
      else {
        iVar9 = new_state;
        if (9 < fifo_info[0]) {
          SI4455_read_rx_fifo(10);
          unit_ID = (uint)rx_fifo_data[0];
          if ((~((uint)rx_fifo_data[8] +
                (uint)rx_fifo_data[7] + (uint)rx_fifo_data[6] + (uint)rx_fifo_data[5] + (uint)rx_fifo_data[4] + (uint)rx_fifo_data[3] + (uint)rx_fifo_data[2] + unit_ID + (uint)rx_fifo_data[1] + 1) &
              0xff) == (uint)rx_fifo_data[9]) {
            if (unit_ID - 1 < 0x20) {
              if (async_entries < 0x20) {
                mask_temp = 1 << (unit_ID - 1 & 0x1f);
                iVar9 = new_state;
                if ((mask_temp & async_mask) == 0) {
                  printk("async request received code: %d\n");
                  uVar24 = unit_ID;
                  iVar9 = async_wp * 0x10;
                  async_mask = async_mask | mask_temp;
                  _async_status = _async_status + 1;
                  *(int *)(&DAT_00018a10 + unit_ID * 4) = *(int *)(&DAT_00018a10 + unit_ID * 4) + 1;
                  *(uint *)(async_frame + iVar9) = uVar24;
                  *(undefined4 *)(async_frame + iVar9 + 4) = now._4_4_;
                  do {
                    iVar18 = iVar9 + channel;
                    iVar12 = channel + 3;
                    channel = channel + 1;
                    async_frame[iVar18 + 8] = *(si4455_async_frame_t *)(rx_fifo_data + iVar12);
                  } while (channel != 6);
                  async_entries = async_entries + 1;
                  async_wp = async_wp + 1U & 0x3f;
                  __wake_up(0x8104,1,1,0);
                  iVar9 = new_state;
                }
              }
              else {
                printk("async buffers full\n");
                iVar9 = new_state;
              }
            }
            else {
              printk("bad async unit ID #\n");
              iVar9 = new_state;
            }
          }
          else {
            printk("bad checksum: %x, %x, %x , %x, %x, %x, %x, %x, %x, %x\n",unit_ID,(uint)rx_fifo_data[1],(uint)rx_fifo_data[2],(uint)rx_fifo_data[3],(uint)rx_fifo_data[4],(uint)rx_fifo_data[5],
                   (uint)rx_fifo_data[6],(uint)rx_fifo_data[7],(uint)rx_fifo_data[8],(uint)rx_fifo_data[9]);
            iVar9 = new_state;
          }
        }
LAB_000153f8:
        last_state = iVar9;
        hrtimer_forward(timer);
        hVar10 = HRTIMER_RESTART;
        times = times + 1;
      }
    }
LAB_00015550:
    now._0_4_ = (undefined4)((ulonglong)now >> 0x20);
    return hVar10;
  }
  if (new_state != 8) {
    hrtimer_forward(timer,local_30,now._0_4_,now._4_4_,ktime._0_4_,ktime._4_4_);
    hVar10 = HRTIMER_RESTART;
    goto LAB_00015550;
  }
  iVar9 = last_state;
  if (last_state != 0xff) {
    if (last_state != 8) {
      state_change = 1;
      bVar22 = true;
      iVar9 = last_state;
      goto LAB_000136e8;
    }
    goto LAB_0001369c;
  }
  goto LAB_000153f8;
}

