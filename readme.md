# Blink Camera and Sync Module

## RF

* something around 900-915mhz
* SI4455

## API

#### GET /api/v1/version

> http "https://rest-prod.immedia-semi.com/api/v1/version" "token_auth: 696578" "APP-BUILD: ANDROID_546900" "User-Agent: 6.2.5ANDROID_546900" "LOCALE: US"

```json
{
    "code": 103,
    "message": "OK",
    "update_available": false,
    "update_required": false
}
```

#### GET /api/v1/sync_modules/G8T1LN0003157FM7/fw_update

Transfered as octet stream, .tar.gz of firmware (2.13.26)

`
X-Blink-Fw-Signature: RvsNte8P+sQBRsUN6zImrAtNff4a7gDLjGD5m0XbTzI=
X-Blink-Fw-Signature-V2: i+mQLbmKEKo0Tsy5OsLH50mzE/I0lAOoiVptorsZkBA=
`

for serial `G8T1LN0003157FM9`:
`
X-Blink-Fw-Signature: RvsNte8P+sQBRsUN6zImrAtNff4a7gDLjGD5m0XbTzI=
X-Blink-Fw-Signature-V2: ueetuIiCDJ5ER60o596+01B8vMG3moIB1FBnDZ1BpDU=
`

X-Blink-Fw-Signature is hmac sha256 of firmware with a secret of empty string

X-Blink-Fw-Signature-V2 is hmac sha256 of firmware with device specific secret

#### GET /api/v1/accounts/76950/owls/G8T1JX0003012A0E/fw_update

`X-Blink-Fw-Signature: dU0RwDOOFh8L7FNWp/XscAU2iGUm6Yffw0C2UYwkHPo=`

octet stream, binwalk says it's gzip.  Same sha1sum as sync_module firmware

## Android App

https://codeshare.frida.re/@akabe1/frida-multiple-unpinning/

### Notes

private String AES_INFO_STRING = "aesblinkob";
private String HMAC_KEY = "hmacblinkob";

api/retrofit/SyncModuleService.java

isSecureRequest:      if (!(string2.contains("/api/set/key") || string2.contains("/api/get_fw_version") || string2.contains("/api/version")))

## Blink Sync Module 2

fccid 2AF77-H1621502
https://fccid.io/2AF77-H1621502

### Connect to AP

$ nmap 172.16.97.199
```
PORT   STATE SERVICE
53/tcp open  domain
80/tcp open  http
```

#### POST /api/set/ssid

HTTP/1.0  420  Parsing set_ssid request failed

#### GET /api/get_fw_version

```json
{
    "encryption": "2",
    "version": "4.1.11"
}
```

#### GET /api/logs

```
{"prev_sm_ob_log":"NFO  (pd/httpd.c: 446) [21:29:11.257]Server ready to accept connection
INFO  (pd/httpd.c: 500) [21:29:16.900]Connected on HTTP server. Waiting for read request
INFO  (pd/httpd.c: 532) [21:29:16.901]Amount of data received:174
INFO  (pd/httpd.c: 257) [21:29:16.902]Endpoint type:1
INFO  (   ob/ob.c: 328) [21:29:16.902]ob sem site scan
INFO  (  cs/net.c: 237) [21:29:16.903]we version compiled value = 22
INFO  (/aes_imm.c:  55) [21:29:18.905]Encrypting data of size 1344
INFO  (pd/httpd.c: 164) [21:29:18.906]Payload length to write:1392
INFO  (pd/httpd.c: 224) [21:29:18.907]Got data of length 1530 to write to client
```


sample just after boot:
```
INFO  (pp/smapp.c: 340) [21:19:09.626]STARTUP IMMI 29
INFO  (cs/imssl.c:  79) [21:19:09.731]Loading cert into imssl context
INFO  (   ob/ob.c: 216) [21:19:09.732]Initializing OB SEM
INFO  (   ob/ob.c: 282) [21:19:09.733]Encryption suite set to version 2
INFO  (   ob/ob.c: 700) [21:19:09.733]Starting AP mode
INFO  (   ob/ob.c:1201) [21:19:09.834]Command to run: wifi
INFO  (   ob/ob.c:1217) [21:19:12.338]Command was run: wifi Status: 0
INFO  (   ob/ob.c:1201) [21:19:12.339]Command to run: /root/apps/led_controller/led_controller 24
INFO  (   ob/ob.c:1217) [21:19:12.339]Command was run: /root/apps/led_controller/led_controller 24 Status: 0
INFO  (   ob/ob.c: 764) [21:19:12.340]AP mode active
INFO  (pd/httpd.c: 446) [21:19:12.341]Server ready to accept connection
INFO  (  cs/net.c: 237) [21:19:14.471]we version compiled value = 22
INFO  (ndpoints.c: 304) [21:19:16.473]Log length 840 840
```

ERROR (   ob/ob.c:1043) [21:20:29.963]Unauthorized request. Session key not set!!!

INFO  (pd/httpd.c: 257) [21:21:04.316]Endpoint type:6
INFO  (/aes_imm.c: 106) [21:21:04.316]AES decrypt of block len 48
INFO  (/aes_imm.c: 157) [21:21:04.317]AES decrypt return len: 32

#### GET /api/ssids

>http http://172.16.97.199/api/ssids "Cookie: nOWXBssl2MT3fn-kKV98yQ" | xxd -p 


`Content-Encoding: encrypted`
`X-Blink-Encrypted: 2`

```
3663a2d2c88264fe56c8e0d54ae3bd706dfe5f6c40f4e6a8a8fdc4283646
4b6daf40811b3ebbf3c96cf8401f885dc1a79088cbdff40c40ad3948526c
a2e0f3d28a3f43f8ea11cfe87bc59f515f046e07481a00a7a839357a9387
824f9a61ae01e072de4f5ff87d68d4fd80fea67d26aaeb37c3eb7a73e22d
731e71ed12d26d531f569c5ffd8db39b1dc632cde0f4dba92ecf59142024
6da5e57d5320db87a7f378790843348a0f89f9f14de333790ac5be83c2ed
59bd1aca02e2eb48600838ceae95cd1e758477fea1e03fccc56cd6439349
a4e18b84ca4882220c652b5392fe65b7b173d5ca5d1565fe1c11e488968e
d7d98dd73e42a4cad56ee987e13f312c1d4e49ea5870e1bad8dcd38b335a
428db09a427a7437b6cd636db0686dfe63fa99081a8e51b7fc2f8345a2a4
2a8593c41fb83634cfe93ebc8b09dececc66918fd63fa50cbdfacbc129c7
ba9be2c35e2d1bfae56edc01f5ef0cd755f2090ea72a97ca1f3589487dd0
2ff2f561865a280bc35f54bffc3baa06929d69c3b4e3b794d6fb58c7eb3e
9841f900167c4ed6730c0890f938ea803e830e50438911ce6cb4b718eeed
354df03aba32720739bd16f329ee36b10444e5725841bfdbdd218917a626
0fe4c8712e0547355b4531a846f691b534ac86d6a035692afb3e8e822fde
73c820833dd71b05b0d8e056daa475aaa77fc6e57688d0faa9809d547227
29b7785b6b13fddce447c74a23787df69b63b9b0ff28b95e56632d4bfc89
5cf59768d166f38de2cb28e7eb98f71f8c95cf9bd4c1a8c4ef53666158dc
db1f8e6bc7e83910b980a99ccd847f5bb39243abdb54e7979cdee45fb23b
ebb98ed73b5890df6624bd26db6b4d923c89fa29e27a44d293e701e8b249
e382ad3d9f1cc79302f536d63c637647332b1d06aaec87db472b86a57127
a5df32f209c5746db079d771efde9deed5c4e64348b3546f6a7ca7cf2e08
2a1c1745816b991bf5b72a373c2d73933740c4f3ba82988e4eddbc2ccdb7
f07e1b63ebcb413b54cf2ef8a3a18fd8891ccf6ac95eede03aebf9d01a16
12f7bbc3d0c21b9f1e06db12cbd145267ca80fc08f808f57eda64d65054e
4ac30b49aed0ff2c71b0fb98f5ae447381784f7f9ced71d38452340d8175
7a2612f84fe85140744532764f67d05a2ce490b6a0638b4b643dc005192c
eec56dc97e58560b278151b3eabd9c1a17efcdb1677354f7f0674169f6f8
5b03a98fb5eb4594c039f79a4064d6f5ab36828b98af19088ae28584bbfb
76354340ed4d6d5289ddba9c6546b34d483938bf45a1ed0d387501d5ccfd
00b63762f85c11a2e343f87c9693e884b121dc1ea4d0ce4b95b1135c9c46
f3e275300f2cff3371e02fb2974caef57bf1499723e8f9720100c5f990bf
0645c0442ad8edf3d823d933f97ffce2734ba58a53b2914f62d8d15aea19
fc45d8bb4bf346552318ef172fcad1343e8a1568ef490857a21501f3483f
2a4969409d64c40b7fb102eb29c7ee4943325bac8231a356c8bd954d5288
799f3c7b7e030db67b684a21c26fa74baf734bf187c6f60c39ca987ee494
877c6bf887c243a1e5ad4878c2912be1c46f0b16f7fd905e3648820f64e7
ccf0b3d0b678746da23f901fdad1115d39c86b66ed36e6c17ac6f64dd20f
980086c80eeecd686a153e138b76b54b9217c5e400bf713786a88059673c
67a0cb8595196b1c442e3cd9248d942621da8c414b47cda48777aaeb32f1
caf1eda1a2b6570d69f7590e47deb77a8ef5571c72aacf1fdcac46c62646
92c11acc29bd6f7377ab8642e19cb7f27b416c6829242f69db88484a32a3
713032f6e1c14369f1b8138a114bf4b4010c38d0391d5d65d274bc3eee65
e0cbca264ea4b822b5554de062a09c4603642b9b4ba563b440ff7072f2b4
831dfa3a6202cbe48e22b0434e78ab27be6773821f3274c28b6646775810
384355f95329263d513200a0a54f980bb251604c410eec60fadac7e65b38
5813cd3732b94fc3b3e2e3acad6d1d93733a32a70a3f26b71ed64043e81d
1edc709a94f15d3beb11327b4d19b2bfff9cf7e9a5395cb5943e37c41bd3
f8a65460d5fbb3d4595c27ab53a1030b7025da76dd01412726e2a3287605
45bf94dfe04415f0f057855d55b32a1ee11b87c4

```

Decrypted:
```json
{"serial_number":"G8T1LN0003157FM7","manual_ssid":true,"version":"4.1.11","access_points":[{"ssid":"Oxford","encryption":"WPA2","channel":"6","quality":"70\/70","mac_address":"f4:92:bf:8f:db:42","signal":"-37"},{"ssid":"407","encryption":"WPA2","channel":"6","quality":"70\/70","mac_address":"f6:92:bf:9f:db:42","signal":"-37"},{"ssid":"ProwlingHamster","encryption":"WPA2","channel":"6","quality":"63\/70","mac_address":"60:38:e0:ca:cf:f1","signal":"-47"},{"ssid":"NameOfNetwork","encryption":"WPA2","channel":"7","quality":"52\/70","mac_address":"be:63:77:b3:85:62","signal":"-58"},{"ssid":"TP-Link_FE3C","encryption":"WPA2","channel":"4","quality":"34\/70","mac_address":"d8:0d:17:4d:fe:3b","signal":"-76"},{"ssid":"Park Ave Secured","encryption":"WPA2","channel":"11","quality":"31\/70","mac_address":"c0:56:27:1b:47:e4","signal":"-79"},{"ssid":"3f","encryption":"WPA2","channel":"11","quality":"31\/70","mac_address":"78:f2:9e:12:84:b8","signal":"-79"},{"ssid":"Centurylink9999","encryption":"WPA2","channel":"1","quality":"28\/70","mac_address":"02:6a:e3:f5:1e:2d","signal":"-82"},{"ssid":"CBCI-81CE-2.4","encryption":"WPA2","channel":"6","quality":"26\/70","mac_address":"20:25:64:a0:f5:88","signal":"-84"},{"ssid":"MikeyMan402","encryption":"WPA2","channel":"11","quality":"21\/70","mac_address":"10:13:31:3c:0c:05","signal":"-89"},{"ssid":"Alcove","encryption":"None","channel":"9","quality":"18\/70","mac_address":"84:16:f9:2b:dc:5a","signal":"-92"}]}
```

#### GET /api/version

404

#### POST /api/set/app_fw_update

Conflicting into between api class and actual activity
Expected header according to android: X-Blink-FW-Signature

`"Content-Type", "application/json"` seems surprising
`X-V2-Blink-FW-Signature`

Tarball, not encrypted, needs to start with newline (`\n`) because of some weird http header parsin in smapp

#### POST /api/set/key

empty request: HTTP/1.0 420 Previous endpoint failed

pipe encrypted session key to it to set session key used to produce 'aesKey' for endpoint encryption (logs/ssids/etc).  Encrypted session key is AES-128-CBC, no padding, with the first 16 bytes as iv, next 16 bytes session key (looks like a base64 string), then fixed string `immediasemisyncm` to validate it was successfully decrypted.

—

> decode64 VN+gLeHsR2Ad84XpL5tW9Xr0yZYM6ASPo2UkKA2JCi5xTy9X7wqtqJTKdgGa6aSH | http -v POST http://172.16.97.199/api/set/key

Corresponds to this aesKey = Buffer.from('50cf33c9ea38111d03c929799fefd5fc', 'hex')

—

#### POST /api/set/ssid

##### First attempt
```
INFO  (pd/httpd.c: 532) [22:18:04.802]Amount of data received:403
INFO  (pd/httpd.c: 257) [22:18:04.803]Endpoint type:5
INFO  (   ob/ob.c:1201) [22:18:04.904]Command to run: rm -f /root/config/auth/sm_id
INFO  (   ob/ob.c:1217) [22:18:04.904]Command was run: rm -f /root/config/auth/sm_id Status: 0
INFO  (   ob/ob.c: 357) [22:18:04.918]SSID info received!
ERROR (ndpoints.c: 427) [22:18:04.919]Couldn't verify hmac. Abort set ssid
ERROR (   ob/ob.c: 364) [22:18:04.919]Could not parse ssid info. Aborting
```

##### Second attempt
```
INFO  (   ob/ob.c: 357) [22:26:55.276]SSID info received!
INFO  (ndpoints.c: 430) [22:26:55.276]HMAC verified
INFO  (/aes_imm.c: 106) [22:26:55.277]AES decrypt of block len 208
INFO  (/aes_imm.c: 157) [22:26:55.278]AES decrypt return len: 192
ERROR (ndpoints.c: 494) [22:26:55.278]Parser failed with -11
ERROR (   ob/ob.c: 364) [22:26:55.279]Could not parse ssid info. Aborting
INFO  (pd/httpd.c: 164) [22:26:55.280]Payload length to write:0
INFO  (pd/httpd.c: 224) [22:26:55.280]Got data of length 84 to write to client
INFO  (pd/httpd.c: 228) [22:26:55.281]Server write complete. Wrote 84 bytes

```

```
{"prev_sm_ob_log":") [22:43:41.510]ping attempt 9
INFO  (   ob/ob.c:1201) [22:43:41.511]Command to run: ping -c 1 8.8.8.8 > /tmp/ping_out
INFO  (   ob/ob.c:1217) [22:43:41.511]Command was run: ping -c 1 8.8.8.8 > /tmp/ping_out Status: 256
INFO  (   ob/ob.c: 945) [22:43:41.512]no connection!
INFO  (   ob/ob.c: 925) [22:43:42.520]ping attempt 10
INFO  (   ob/ob.c:1201) [22:43:42.521]Command to run: ping -c 1 8.8.8.8 > /tmp/ping_out
INFO  (   ob/ob.c:1217) [22:43:42.534]Command was run: ping -c 1 8.8.8.8 > /tmp/ping_out Status: 256
INFO  (   ob/ob.c: 945) [22:43:42.535]no connection!
INFO  (   ob/ob.c: 925) [22:43:43.536]ping attempt 11
INFO  (   ob/ob.c:1201) [22:43:43.537]Command to run: ping -c 1 8.8.8.8 > /tmp/ping_out
INFO  (   ob/ob.c:1217) [22:43:43.538]Command was run: ping -c 1 8.8.8.8 > /tmp/ping_out Status: 256
INFO  (   ob/ob.c:1201) [22:43:43.538]Command to run: dmesg|tail -n 10 > /tmp/latest_dmesg.log
INFO  (   ob/ob.c:1217) [22:43:43.539]Command was run: dmesg|tail -n 10 > /tmp/latest_dmesg.log Status: 0
INFO  (   ob/ob.c:1008) [22:43:43.540]Password correct
INFO  (   ob/ob.c: 925) [22:43:44.546]ping attempt 12
INFO  (   ob/ob.c:1201) [22:43:44.547]Command to run: ping -c 1 8.8.8.8 > /tmp/ping_out
INFO  (   ob/ob.c:1217) [22:43:44.648]Command was run: ping -c 1 8.8.8.8 > /tmp/ping_out Status: 256
INFO  (   ob/ob.c: 945) [22:43:44.648]no connection!
INFO  (   ob/ob.c: 925) [22:43:45.650]ping attempt 13
INFO  (   ob/ob.c:1201) [22:43:45.651]Command to run: ping -c 1 8.8.8.8 > /tmp/ping_out
INFO  (   ob/ob.c:1217) [22:43:45.726]Command was run: ping -c 1 8.8.8.8 > /tmp/ping_out Status: 256
INFO  (   ob/ob.c: 945) [22:43:45.727]no connection!
INFO  (   ob/ob.c: 925) [22:43:46.628]ping attempt 14
INFO  (   ob/ob.c:1201) [22:43:46.629]Command to run: ping -c 1 8.8.8.8 > /tmp/ping_out
INFO  (   ob/ob.c:1217) [22:43:46.629]Command was run: ping -c 1 8.8.8.8 > /tmp/ping_out Status: 256
INFO  (   ob/ob.c: 952) [22:43:46.630]No connection to the internet. Stopping pinging
ERROR (   ob/ob.c: 428) [22:43:46.631]Can't Connect to Internet
INFO  (   ob/ob.c:1201) [22:43:46.732]Command to run: wifi
INFO  (   ob/ob.c:1217) [22:43:47.133]Command was run: wifi Status: 0
INFO  (   ob/ob.c:1201) [22:43:47.133]Command to run: /root/apps/led_controller/led_controller 1
INFO  (   ob/ob.c:1217) [22:43:47.134]Command was run: /root/apps/led_controller/led_controller 1 Status: 0
INFO  (   ob/ob.c:1201) [22:43:47.135]Command to run: rm /root/config/auth/sm_id
INFO  (   ob/ob.c:1213) [22:43:47.135]rm: can't remove '/root/config/auth/sm_id': No such file or directory
INFO  (   ob/ob.c:1217) [22:43:47.169]Command was run: rm /root/config/auth/sm_id Status: 256
INFO  (   ob/ob.c: 899) [22:43:47.169]Failure mode active
ERROR (   ob/ob.c: 554) [22:44:12.389]SM still unconnected to the internet. Aborting onboarding
INFO  (pp/smapp.c:1207) [22:48:15.363]TMP 0/7379 blocks (0%) 7455/0 inodes
INFO  (pp/smapp.c:1214) [22:48:15.364]FW  4.1.7 2020-02-27 10m 0s 629
INFO  (pp/smapp.c:1207) [22:53:15.507]TMP 0/7379 blocks (0%) 7455/0 inodes
INFO  (pp/smapp.c:1214) [22:53:15.507]FW  4.1.7 2020-02-27 15m 0s 929
INFO  (pp/smapp.c:1207) [22:58:15.436]TMP 0/7379 blocks (0%) 7455/0 inodes
INFO  (pp/smapp.c:1214) [22:58:15.437]FW  4.1.7 2020-02-27 20m 0s 1229
```


#### GET /api/dmesg
Found in smapp

404

#### GET /api/sm_status
Found in smapp

```json
{"Connection_status":"0"}
```

#### GET /api/info
Found in smapp

```json
{"serial_number":"G8T1LN0003157FM7w���"}
```

has some weird binary at the end of the serial ascii

### Firmware

Downloaded a copy of old firmware (2.13.26)

Device factory resets to 4.1.11
Connecting device to cloud updates to (As for Feb 2021) 4.2.22

#### General notes, interesting strings

INFO  (est/rest.c: 560) [06:32:50.912]Failed rest-hw-u021.immedia-semi.com (0) wait 2s (106)
INFO  (est/rest.c: 506) [06:32:52.980]/api/v1/sm/status?serial=G8T1LN0003157FM7&fw_version=4.2.12&onboarding=false



```
bytes of /dev/mtd6
skip=12 count=6 mac
skip=18 count=9 serial
skip=27 count=1 pwd_len
skip=28 count=$len password
skip=28 count=2 pwd_len
skip=30 count=$len password
skip=94 count=1 valid_new_key
skip=95 count=16 new_key
skip=111 count=1 dsn_valid
skip=112 count=16 serial_dsn
skip=128 count=1 region
skip=4096 count=61440 calibration
```

#### root/apps/connection/smapp

ELF 32-bit MSB executable, MIPS, MIPS32 rel2 version 1 (SYSV), dynamically linked, interpreter /lib/ld-uClibc.so.0, with debug_info, not stripped

 * SI4455
   * 48 kbit modulation
 * aCommandToRunS_1
 * validate_fw_bundle
 * IMMI_INSTRUCTION_SM_UPDATE_KEY
 * sem_SmKeyUpdate


#### Shell

I got root on the device, here are some of the commands I ran and their output

#### ./read_password
10

#### ./read_password_v2
mine97shoe

#### ./read_new_key
e56f9988519e89c6298d9bb6debe0cb7

#### cat /root/config/auth/sm_id
124125


#### cat /proc/mtd

```
dev:    size   erasesize  name
mtd0: 00040000 00010000 "u-boot"
mtd1: 00010000 00010000 "u-boot-env"
mtd2: 001b0000 00010000 "kernel"
mtd3: 005e0000 00010000 "rootfs"
mtd4: 00180000 00010000 "rootfs_data"
mtd5: 00010000 00010000 "NVRAM"
mtd6: 00010000 00010000 "ART"
```


### Connected to your AP

When connected to an access point, the device will reach out to a server over https ('rest server'), which will provide a response with a second server ('cs server') hostname.


#### Rest server

https connection.

TODO: example servername, which I think is set in the call to the set ap endpoint


##### /fw/file/%d

1 returns 92K tarball last modified 2015-09-18 21:04:43
182 is a custom "IMBL" (IMmedia BundLe?) format.  might be encrypted



##### /network/%d/syncmodule/%d/ip_address

public ip address in dotted decimal

#### CS Server

Rest server api call returns a hostname and port, but the port is ignored, always 443.  Although 443 and using TLS, the connection is not HTTPS.  Using a custom binary protocol
Must use client certs from device (/root/apps/connection/[`server.pem`|`server_priv.pem`]).

##### CS Protocol

`00 00 00 1e 00 00 00 10 ss ss ss ss ss ss ss ss ss ss ss ss ss ss ss ss uu uu uu uu uu uu 00 00 00 16 aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa`

|offset | length | value | meaning |
|----|---|----|
|0 | 4 | 30 | length of something? |
|4 | 4 | 16 | length of serial number that follows|
|8 | 16 | ss... | serial number in ascii |
|24 | 1 | 00 | not sure |
|25 | 1 | 01 | not sure |
|26 | 1 | e4 | not sure |
|27 | 1 | dd | not sure |
|28 | 1 | 03 | not sure, hardcoded |
|29 | 1 | 06 | not sure, hardcoded |
|30 | 22 | aa... | auth token, I assume set by set AP request|
|52 | long | 00's | crap tone of zeros |
| -2 | 1 | 4 | hardcoded, maybe an end-of-message? |
| -1 | 1 | 4 | hardcoded, same |

| value | command | meaning |
| ---- | ---- | ---- |
| 0x0d |  IMMI_INSTRUCTION_NEW_SERVER_ADDR ||
| 0x1b | New Table Update Sequence | |
| 0x36 | IMMI_INSTRUCTION_SM_UPDATE_KEY | Introduced in 2.13.26 (aka 443) |
| 0x35 | IMMI_INSTRUCTION_LFR_STATS | |
| 0x41 |  LS CONNECT CONFIG ||

## Blink Indoor Camera

fccid 2AF77-H2041670
https://fccid.io/2AF77-H2041670

