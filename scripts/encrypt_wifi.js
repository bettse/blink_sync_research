const crypto = require('crypto');

const AES_INFO_STRING = "aesblinkob"
const HMAC_KEY = "hmacblinkob"

const exampleSessionKey = 'QGaUKGphowU9jXhV'
const examples = {
  sm_status: Buffer.from('7172773997c21664c935a885d786a0b7ca05c4f39ce6f73106ec539178f965eb8e0c0fd0ddb4dd79f3ee8a9c2d101686deea3076031efc4ed837e556ad9259a231a394256abe823559b201da2ae962b0', 'hex')
};

function encrypt_with_session_key(sessionKey, cleartext) {
  const aesKey = Buffer.from(crypto.hkdfSync('sha256', sessionKey, '', AES_INFO_STRING, 16))
  const hmacKey = Buffer.from(crypto.hkdfSync('sha256', sessionKey, '', HMAC_KEY, 32))

  const iv = crypto.randomBytes(16);

  const cipher = crypto.createCipheriv('AES-128-CBC', aesKey, iv)
  const ciphertext = Buffer.concat([cipher.update(cleartext, 'utf8'), cipher.final()])

  const hmac_over = Buffer.concat([iv, ciphertext])

  const hmac = crypto.createHmac('sha256', hmacKey)
  hmac.update(hmac_over);
  const calculated_hmac = hmac.digest();
  return Buffer.concat([hmac_over, calculated_hmac])
}

const message = {
  auth: 'nOWXBssl2MT3fn-kKV98yQ',
  dns: '', // 'name'
  domain: 'xxxx', //tier
  ssids: [{
    ssid: 'NameOfNetwork',
    password: '',
    encryption: 'none',
    device_id: 0, //only for OWL
    network_id: 0,
  }],
}

const encryptedMessage = encrypt_with_session_key(exampleSessionKey, JSON.stringify(message))
console.log(encryptedMessage.toString('hex'))

