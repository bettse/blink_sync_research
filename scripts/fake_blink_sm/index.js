const crypto = require('crypto');
const express = require('express')

const AES_INFO_STRING = "aesblinkob"
const HMAC_KEY = "hmacblinkob"

const app = express()
const version = '3.0.0'
// "4.1.11" // original
// "4.2.12" // first update


let sessionKey = 'GcGF8o2olrtU8ZKv';

function encrypt_with_session_key(sessionKey, cleartext) {
  const aesKey = Buffer.from(crypto.hkdfSync('sha256', sessionKey, '', AES_INFO_STRING, 16))
  const hmacKey = Buffer.from(crypto.hkdfSync('sha256', sessionKey, '', HMAC_KEY, 32))

  const iv = crypto.randomBytes(16);

  const cipher = crypto.createCipheriv('AES-128-CBC', aesKey, iv)
  const ciphertext = Buffer.concat([cipher.update(cleartext, 'utf8'), cipher.final()])

  const hmac_over = Buffer.concat([iv, ciphertext])

  const hmac = crypto.createHmac('sha256', hmacKey)
  hmac.update(hmac_over);
  const calculated_hmac = hmac.digest();
  return Buffer.concat([hmac_over, calculated_hmac])
}


//app.use(express.json()); //Used to parse JSON bodies
//app.use(express.raw({type: 'application/x-www-form-urlencoded'}));
//app.use(express.raw({type: 'application/octet-stream'}));
app.use(express.raw());


app.get('/api/get_fw_version', (req, res) => {
  console.log('request to', req.path)
  const fw_version = {
    encryption: "2",
    version,
  };
  res.send(fw_version);
})

app.post('/api/set/key', (req, res) => {
  console.log('request to', req.path)
  console.log('set/key', req.headers, req.body.toString('hex'))
  const encrypted_session_key = req.body;
  //decrypt
  // sessionKey = ...
  res.status(200).send('');
})

app.get('/api/ssids', (req, res) => {
  console.log('request to', req.path)
  const ssids = {
    serial_number:"G8T1LN0003157FM7",
    manual_ssid:true,
    version,
    access_points: [{
      ssid:"Oxford",
      encryption:"WPA2",
      channel:"6",
      quality:"70\/70",
      mac_address:"f4:92:bf:8f:db:42",
      signal:"-37"
    }]
  }
  res.status(200).send(encrypt_with_session_key(sessionKey, JSON.stringify(ssids)))
})

app.post('/api/set/ssid', (req, res) => {
  console.log('request to', req.path)
  console.log('set/ssid', req.headers, req.body, req.body.toString('hex'), JSON.stringify(req.body))
  res.status(200).send('')
})

app.get('/api/logs', (req, res) => {
  console.log('request to', req.path)
  const logs = {
    prev_sm_ob_log: ""
  }
  res.status(200).send(encrypt_with_session_key(sessionKey, JSON.stringify(logs)))
})

app.get('/api/sm_status', (req, res) => {
  console.log('request to', req.path)
  const status = {
    connection_status: "0"
  };
  res.status(200).send(encrypt_with_session_key(sessionKey, JSON.stringify(status)));
})

app.get('/api/info', (req, res) => {
  console.log('request to', req.path)
  const info = {
    serial_number: "G8T1LN0003157FM7"
  }
  res.status(200).send(encrypt_with_session_key(sessionKey, JSON.stringify(info)))
})


app.get('/api/version', (req, res) => {
  console.log('request to', req.path)
})

app.post('/api/set/app_fw_update', (req, res) => {
  console.log('set/app_fw_update', req.headers, req.body.toString('hex'))
  console.log('request to', req.path)
  res.status(200)
})

app.get('*', (req, res) => {
  console.log('get request to', req.path)
  res.status(404)
});

app.post('*', (req, res) => {
  console.log('post request to', req.path)
  res.status(404)
});

app.listen(80, () => {
  console.log(`express listening`);
})

