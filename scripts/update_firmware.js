//http -v POST http://172.16.97.199/api/set/app_fw_update "X-V2-Blink-FW-Signature: AQ26qQ/W16V5nAT09pL1mrBuezsHhoYON54cZcl9o2w=" < firmware.tar.gz

const fs = require('fs');
const crypto = require('crypto');
const axios = require('axios');

const blink_sm_ip = '172.16.97.199'
const blink_fw_api = '/api/set/app_fw_update'


const dsn = 'G8T1LN0003157FM7';
const secret = Buffer.from('e56f9988519e89c6298d9bb6debe0cb7', 'hex');

function hmac(firmware, secret) {
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(firmware);
  return hmac.digest();
}

async function main(filename) {
  const firmware = fs.readFileSync(filename)

  // TODO: build tarball
  // * ./root/utils/sm_update
  // * ./root/.walnut/version
  // * sm.tar.gz
  // * sm.sig

  return await upload(firmware)
}

async function upload(firmware) {
  const headers = {
    'X-V2-Blink-FW-Signature': hmac(firmware, secret).toString('base64'),
  }

  // SM looks for newline before tarball data
  const mod_firmware = Buffer.concat([Buffer.from('\n', 'ascii'), firmware])
  const url = `http://${blink_sm_ip}${blink_fw_api}`

  const response = await axios.post(url, mod_firmware, {headers});

  return response.data;
}

if (process.argv.length > 2) {
  const filename = process.argv[2];
  main(filename).then(console.log).catch(console.error)
} else {
  console.log('firmware_update <firmware>');
}

