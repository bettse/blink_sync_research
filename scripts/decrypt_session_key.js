const crypto = require('crypto');

//const encrypted_session_key = Buffer.from('nYMZqoH8LVaO4HAUc4d8Oo+WXyhDKm8FcyYnKBI7+DNR41GJ1jY1+nYwJ1ENLSBB', 'base64');

const encrypted_session_key_v2 = Buffer.from('VN+gLeHsR2Ad84XpL5tW9Xr0yZYM6ASPo2UkKA2JCi5xTy9X7wqtqJTKdgGa6aSH', 'base64');

const known_session_key = 'QGaUKGphowU9jXhV'
const immediasemisyncm = 'immediasemisyncm'

const aesKey = Buffer.from('e56f9988519e89c6298d9bb6debe0cb7', 'hex')

const cleartext = decryptSessionKey(encrypted_session_key_v2, aesKey).toString();
const session_key = cleartext.slice(0, 16);
const known = cleartext.slice(16);

if ( immediasemisyncm === known) {
  if ( known_session_key === session_key) {
    console.log('success!  found session key', session_key.toString())
  }
} else {
  console.log('did not contain known string', cleartext)
}


//Verify known text
//compare to expected session_key


function decryptSessionKey(encrypted_session_key_v2, aesKey) {
  const iv = encrypted_session_key_v2.slice(0, 16)
  const ciphertext = encrypted_session_key_v2.slice(16)

  const decipher = crypto.createDecipheriv('AES-128-CBC', aesKey, iv)

  decipher.setAutoPadding(false); // !!!!!!!!!add this line!!!!!!!!!!
  try {
    const receivedPlaintext = decipher.update(ciphertext)
    return Buffer.concat([receivedPlaintext, decipher.final()])
  } catch (e) {
    console.error('Authentication failed!', e);
    return Buffer.alloc(0)
  }
}
