const axios = require('axios');

const email = "barakobama+1@whitehouse.gov"
const password = "White_House01"

const api = {
  register: 'https://rest-prod.immedia-semi.com/api/v5/account/register',
  login: 'https://rest-prod.immedia-semi.com/api/v5/account/login',
}

async function main() {

  const { account_id, tier, pin} = await register();
  console.log({account_id, tier, pin})
  const { token } = await login();
  console.log({token})
  const response = await verify(tier, account_id, token, pin);
  console.log(response)

}

async function register() {
  const response = await axios.post(api.register, {email, password, password_confirm: password})

  const { account, auth } = response.data;
  const { account_id, tier } = account;
  const { token } = auth

  return { account_id, tier, pin: token}
}

async function login() {
  const response = await axios.post(api.login, {email, password})
  const { auth } = response.data;
  const { token } = auth;
  return { token }
}

async function verify(tier, account_id, token, pin) {
  const verify = `https://rest-${tier}.immedia-semi.com/api/v4/account/${account_id}/pin/verify`

  const response = await axios.post(verify, {pin}, {headers: {'token-auth': token}})
  return response.data
}

main().then(console.log)
