const crypto = require('crypto');
const fs = require("fs");

const AES_INFO_STRING = "aesblinkob"
const HMAC_KEY = "hmacblinkob"

const exampleSessionKey = 'QGaUKGphowU9jXhV'
const examples = {
  sm_status: Buffer.from('7172773997c21664c935a885d786a0b7ca05c4f39ce6f73106ec539178f965eb8e0c0fd0ddb4dd79f3ee8a9c2d101686deea3076031efc4ed837e556ad9259a231a394256abe823559b201da2ae962b0', 'hex')
};

function decrypt_with_session_key(sessionKey, encrypted) {
  const aesKey = Buffer.from(crypto.hkdfSync('sha256', sessionKey, '', AES_INFO_STRING, 16))
  const hmacKey = Buffer.from(crypto.hkdfSync('sha256', sessionKey, '', HMAC_KEY, 32))

  const iv = encrypted.slice(0, 16);
  const ciphertext = encrypted.slice(iv.length, encrypted.length - 32);
  const hmac_over = Buffer.concat([iv, ciphertext])
  const msg_hmac = encrypted.slice(hmac_over.length)

  const hmac = crypto.createHmac('sha256', hmacKey)
  hmac.update(hmac_over);
  const calculated_hmac = hmac.digest();

  if (Buffer.compare(msg_hmac, calculated_hmac) !== 0) {
    console.log(`msg hmac ${msg_hmac.toString('hex')} does not match calculated ${calculated_hmac.toString('hex')}`)
    return
  }


  const decipher = crypto.createDecipheriv('AES-128-CBC', aesKey, iv)
  const receivedPlaintext = decipher.update(ciphertext);
  try {
    return Buffer.concat([receivedPlaintext, decipher.final()])
  } catch (err) {
    console.error('Authentication failed!');
    return;
  }
}

// Support piping from stdin
if (process.argv.length > 2) {
  const stdinBuffer = fs.readFileSync(0); // STDIN_FILENO = 0
  const [node, script, firstParam] = process.argv;
  const encrypted = Buffer.from(stdinBuffer.toString(), 'hex');

  const cleartext = decrypt_with_session_key(firstParam, encrypted);
  if (cleartext) {
    console.log(cleartext.toString())
  } else {
    console.error('error decrypting')
  }
}

// Node 15 has hkdf built in
//console.log(Buffer.from(crypto.hkdfSync('sha256', 'nOWXBssl2MT3fn-kKV98yQ', '', AES_INFO_STRING, 16)).toString('hex'))
//console.log(Buffer.from(crypto.hkdfSync('sha256', 'QGaUKGphowU9jXhV', '', AES_INFO_STRING, 16)).toString('hex'))

function hkdf(sessionKey, length, seed) {
  const hmac = crypto.createHmac('sha256', Buffer.alloc(32).fill(0));
  hmac.update(sessionKey);
  const output = hmac.digest();

  const hmac2 = crypto.createHmac('sha256', output);
  hmac2.update(seed + '\1')

  return hmac2.digest().slice(0, length).toString('hex')
}
