const crypto = require('crypto');
const fs = require("fs");

const dsn = 'G8T1LN0003157FM7';
const sessionKey = 'QGaUKGphowU9jXhV'
const sync_module_id = 90108

const secret = Buffer.from('e56f9988519e89c6298d9bb6debe0cb7', 'hex');

const signatures = {
  'G8T1LN0003157FM7': [
    Buffer.from('RvsNte8P+sQBRsUN6zImrAtNff4a7gDLjGD5m0XbTzI=', 'base64'),
    Buffer.from('i+mQLbmKEKo0Tsy5OsLH50mzE/I0lAOoiVptorsZkBA=', 'base64'),
  ],
  'G8T1LN0003157FM9': [
    Buffer.from('RvsNte8P+sQBRsUN6zImrAtNff4a7gDLjGD5m0XbTzI=', 'base64'),
    Buffer.from('ueetuIiCDJ5ER60o596+01B8vMG3moIB1FBnDZ1BpDU=', 'base64'),
  ],
}

const filename = process.argv.length > 2 ? process.argv[2] : './sync_module_fw.tar.gz'
const firmware = fs.readFileSync(filename)

function hmac(firmware, secret) {
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(firmware);
  return hmac.digest();
}

if (filename === './sync_module_fw.tar.gz') {
  // V1 uses empty string
  console.log({secret: '', hmac: hmac(firmware, '').toString('base64'), sig: signatures[dsn][0].toString('base64')})
  console.log({secret: secret.toString('hex'), hmac: hmac(firmware, secret).toString('hex'), target: signatures[dsn][1].toString('hex')})
} else {
  console.log('reading', filename)
  console.log('X-V2-Blink-FW-Signature:', hmac(firmware, secret).toString('base64'))
}
