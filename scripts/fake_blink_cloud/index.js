const net = require('net');
const tls = require('tls');
const fs = require('fs');
const https = require('https');

const express = require('express')
const proxy = require('express-http-proxy');

const privateKey  = fs.readFileSync('key.pem', 'utf8');
const certificate = fs.readFileSync('cert.pem', 'utf8');

const credentials = {key: privateKey, cert: certificate};

const certs = [
  fs.readFileSync('./certs/cert0.pem'),
  fs.readFileSync('./certs/cert1.pem'),
  fs.readFileSync('./certs/cert2.pem'),
  fs.readFileSync('./certs/cert3.pem'),
  fs.readFileSync('./certs/cert4.pem'),
  fs.readFileSync('./certs/cert5.pem'),
  fs.readFileSync('./certs/cert6.pem'),
  fs.readFileSync('./certs/cert7.pem'),
  fs.readFileSync('./certs/cert8.pem'),
  fs.readFileSync('./certs/cert9.pem'),
];

const remote = [{
  ip: '54.71.30.128',
  host: 'rest-hw-u021.immedia-semi.com'
},{
  ip: '52.34.190.40',
  host: 'u021.immedia-semi.com'
}]

function startHttps() {
  const app = express()
  //app.use(express.static('public'))
  //app.use(express.urlencoded()); //Parse URL-encoded bodies
  app.use(express.json()); //Used to parse JSON bodies
  //app.use(express.raw({type: 'application/x-www-form-urlencoded'}));
  //app.use(express.raw({type: 'application/octet-stream'}));

  /*
  app.use(proxy(remote[0].ip, {
    proxyReqBodyDecorator: (bodyContent, srcReq) => {
      console.log('bodyContent', bodyContent.toString('hex'))
      return bodyContent;
    },
    proxyReqOptDecorator: (proxyReqOpts, srcReq) => {
      console.log('proxyReqOptDecorator', proxyReqOpts)
      return proxyReqOpts;
    },
    userResDecorator: (proxyRes, proxyResData, userReq, userRes) => {
      console.log('proxyResData', proxyResData.toString())
      return proxyResData;
    }
  }));
  */

  // sem_CSOpen -> rest_get_ip()
  app.get('/network/:networkId/syncmodule/:syncmoduleId/ip_address', (req, res) => {
    console.log('/network/syncmodule/ip_address');
    res.status(200).send('174.127.219.54');
  });

  app.get('/api/v1/sm/status', (req, res) => {
    console.log('/api/v1/sm/status', req.query);
    const resp = {
      action: 'cs',
      config: {
        ack_duration: 15
      },
      connect_time: 0,
      host: 'cs-u021.immedia-semi.com', //'cs-u021.immedia-semi.com',
      port: 443,
      status: 'ok'
    }
    res.status(200).send(resp);
  });

  app.all('*', (req, res) => {
    console.log(req.header('host'), 'request to', req.path)
    res.status(200)
  });

  const errorHandler = (err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
  }

  const config = {
    ...credentials,
    requestCert: true,
    rejectUnauthorized: false,
  }

  const serverHttps = https.createServer(config, app)

  serverHttps.on('clientError', (err, socket) => {
    const { bytesParsed, code,  reason,  rawPacket } = err;
    // It would be more elegant to have a TlsServer that figures out what is 
    // https vs not and route it, but I couldn't figure that out
    if (code === 'HPE_INVALID_METHOD') {
      const resp = parseRaw(rawPacket);
      if (resp) {
        socket.write(resp);
      }
    } else {
      console.log('err', err);
    }
  });

  serverHttps.on('error', () => {
    console.log('error')
  })

  serverHttps.listen(443, () => {
    console.log('https listening')
  })
}

function parseRaw(packet) {
  console.log('parseRaw', packet.toString('hex'));
  try {
    // first packet from sem_Open
    if (packet.length === 0x7a) {
      // 0000001e00000010473854314c4e30303033313537464d37 00 01 e4 dd 03 06 00000016796e2d4f65445a4b7254345a50357866624578446f41000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000404
      var index = 0;
      const length1 = packet.readUInt32BE(index);
      index += 4 //size of UInt32
      const sn_length = packet.readUInt32BE(index);
      index += 4 //size of UInt32
      const sn = packet.slice(index, index+sn_length);
      index += sn_length;
      const unknown = packet.slice(index, index+6);
      index += 6;
      const auth_length = packet.readUInt32BE(index);
      index += 4;
      const auth_token = packet.slice(index, index + auth_length)
      index += auth_length;
      const rest = packet.slice(index)
      console.log({sn, unknown, auth_token, rest})

      const resp = Buffer.from('0100000004', 'hex') //log: Recv 1 4
      return resp;
    } else if (Buffer.compare(packet, Buffer.from('00000000', 'hex')) === 0) { //ready?

      //00 00 00 00 00 //5 byte header
      //1b 0001 5466 00000001000000000000000000000000000000000000000000000000000000040000000000000000000000
      //00 00 00 00 00
      // 6026e931 /*New Table Update Sequence 1613162801*/ 0000000009
      //00000010 473854314a5830303033303132413045 //camera SN
      //00000010 4673543073712d41543470414f51444a // auth code
      //00000013 473854312d4a5830302d303330312d32413045 // camera name
      //000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000302cf90e87dac0ccb6ea409144a1ba2a3b00e5ab66d366f5a9127f7fa361825ad09768fcfd1c082be2d5a82f2c7a31fbea010000000000000000000000000000000000000000000000000000000000000000000000000000000008376c793351756c63000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000400000000000000000040000154660000000100000000000000000000000000000000000000000000000000000004000000000000000000000000000000000000000000000000000000000000000000000000000000
      /*
       * 00000000  00 00 00 00 00 1b 00 01  54 66 00 00 00 01 00 00  |........Tf......|
       * 00000010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000020  00 00 00 00 00 00 00 00  00 04 00 00 00 00 00 00  |................|
       * 00000030  00 00 00 00 00 00 00 00  00 00 60 26 e9 31 00 00  |..........`&.1..|
       * 00000040  00 00 09 00 00 00 10 47  38 54 31 4a 58 30 30 30  |.......G8T1JX000|
       * 00000050  33 30 31 32 41 30 45 00  00 00 10 46 73 54 30 73  |3012A0E....FsT0s|
       * 00000060  71 2d 41 54 34 70 41 4f  51 44 4a 00 00 00 13 47  |q-AT4pAOQDJ....G|
       * 00000070  38 54 31 2d 4a 58 30 30  2d 30 33 30 31 2d 32 41  |8T1-JX00-0301-2A|
       * 00000080  30 45 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |0E..............|
       * 00000090  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000000a0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000000b0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000000c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000000d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000000e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000000f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000100  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000110  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000120  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000130  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000140  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000150  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000160  00 00 00 00 00 00 00 00  00 00 00 00 00 00 01 00  |................|
       * 00000170  00 00 30 2c f9 0e 87 da  c0 cc b6 ea 40 91 44 a1  |..0,........@.D.|
       * 00000180  ba 2a 3b 00 e5 ab 66 d3  66 f5 a9 12 7f 7f a3 61  |.*;...f.f......a|
       * 00000190  82 5a d0 97 68 fc fd 1c  08 2b e2 d5 a8 2f 2c 7a  |.Z..h....+.../,z|
       * 000001a0  31 fb ea 01 00 00 00 00  00 00 00 00 00 00 00 00  |1...............|
       * 000001b0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000001c0  00 00 00 00 00 00 00 00  00 00 00 00 08 37 6c 79  |.............7ly|
       * 000001d0  33 51 75 6c 63 00 00 00  00 00 00 00 00 00 00 00  |3Qulc...........|
       * 000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000200  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000210  00 00 04 00 00 00 00 00  00 00 00 00 40 00 01 54  |............@..T|
       * 00000220  66 00 00 00 01 00 00 00  00 00 00 00 00 00 00 00  |f...............|
       * 00000230  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000240  04 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000250  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
       * 00000260  00 00 00 00 00 00 00 00                           |........|
      */
      const header = Buffer.from('0000000000', 'hex')
    }


    return Buffer.from('0100000004', 'hex')

  } catch (err) {
    console.log('parseRaw error', packet.toString('hex'), err);
  }
}


// ----- TLS Client to connect to real cloud
function startTlsClient() {
  const clientOptions = {
    key: fs.readFileSync('./remote/server_priv.pem'),
    cert: fs.readFileSync('./remote/server.pem'),
    ca: certs,
    rejectUnauthorized: false,
    // checkServerIdentity: () => { console.log('checkServerIdentity'); return null; },
  }

  const tlsSocket = tls.connect(443, '44.242.10.73', clientOptions, () => {
    console.log('tls client connected', tlsSocket.authorized ? 'authorized' : 'unauthorized');
    tlsSocket.write(Buffer.from('0000001e00000010473854314c4e30303033313537464d370001e4dd030600000016796e2d4f65445a4b7254345a50357866624578446f41000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000404', 'hex'));
  })

  tlsSocket.on('error', (data) => {
    console.log('client error', data)
  })

  var state = 0;
  tlsSocket.on('data', (data) => {
    console.log('client data', data.toString('hex'))

    console.log({state})
    switch (state) {
      case 0:
        tlsSocket.write(Buffer.from('00000000', 'hex'));
        break;
      case 1:
        tlsSocket.write(Buffer.from('0500000000', 'hex'))
        break;
      case 2:
        tlsSocket.write(Buffer.from('00000006342e322e3132000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000674ab93119cec0000000001c0a89605000000000000000000000000ffcd000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000021000402020000000700000000', 'hex'))
        break;
      default:
        console.log('no response')
    }

    state++;
  });
}

//startTlsClient();
startHttps();
